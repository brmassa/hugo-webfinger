## WebFinger for Hugo

[WebFinger](https://webfinger.net) is a method to tell that a given list of social network profiles is about the same person. Similar to services like *Linktree*, but for computers to read.

Currently, it's important for services like [Mastodon](https://joinmastodon.org), the Fediverse and IndieWeb.

This module implements WebFinger protocol into your [Hugo](https://gohugo.io) site. It will create a `.well-known/webfinger` address for your site.

## Install

Inside your site's main config

YAML
```yaml
module:
  imports:
    - path: gitlab.com/brmassa/hugo-webfinger
```

TOML
```toml
[module]
[[module.imports]]
path = "gitlab.com/brmassa/hugo-webfinger"
```

It will be automatically installed as a Hugo module when you build your site or serve it locally.

### Update

To update *only this* module, use the terminal command:
```terminal
hugo mod get -u gitlab.com/brmassa/hugo-webfinger
```

To update *all* modules, use the terminal command:
```terminal
hugo mod get -u
```

## Setup

in the same file as before, in the `Params` section, add:

YAML
```yaml
params:
  webfinger:
    self: ""
    inbox: ""
    webfinger: ""
  authors:
    # Add these info for each author
    - name: Bruno Massa
      picture: /images/my-profile.jpg
```

TOML
```toml
[params.webfinger]
  self = ""
  inbox = ""
  sharedInbox = ""

# Add these info for each author
[[params.authors]]
  name = "Bruno Massa"
  picture = "/images/my-profile.jpg"
```
